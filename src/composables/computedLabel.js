import { ref, computed } from 'vue'

export function useComputedLabel() {
  const label = ref('')
  const computedLabel = computed(() => {
    if (label.value) {
      return '[Label] ' + label.value;
    } else {
      return ''
    }
  })

  return {
    label,
    computedLabel
  }
}

export function useComputedLabel2() {
  const label = ref('')
  const computedLabel = computed(() => {
    if (label.value) {
      return '[Label2] ' + label.value;
    } else {
      return ''
    }
  })

  return {
    label,
    computedLabel
  }
}

export default {
  useComputedLabel,
  useComputedLabel2,
}
