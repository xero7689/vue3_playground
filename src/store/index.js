import { createStore } from 'vuex'

const store = createStore({
  state () {
    return {
      // ORCA API Client
      client: undefined,

      // Google reCAPTCHA ?
      // Used to protect API like upsert information ?
      
      // ORCA API Request Context
      requestBase: {
        projectId: '',
        identityPoolId: '',
        clientId: '',
        clientQuery: '', //?
        token: {
          idToken: '',
          raptToken: '',
        },
        sso: {},
        form: {},
        utm: {},
      },
      // For handling general exception
      exception: {
        phaseA: {
          api_call: '',
          statusCode: '',
          error_msg: '',
          error_time: ''
        },
      },
      helpdesk: { common: '', client: '' },
      retryAttempt: 0,
    }
  },
  mutations: {
  }
})

export default store;
